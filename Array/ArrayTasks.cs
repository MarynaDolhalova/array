using System;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static void ChangeElementsInArray(int[] nums)
        {
            for (int i = 0; i <= (nums.Length / 2) - 1; i++)
            {   
                if (nums.Length == 0) 
                { 
                    break; 
                } 
                else 
                {
                    if (nums[i] % 2 == 0 && nums[nums.Length - i - 1] % 2 == 0)
                    {
                        int temp = nums[nums.Length - i - 1];
                        nums[nums.Length - i - 1] = nums[i];
                        nums[i] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            int result = 0;
            int index = 0;
            int max = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums.Length == 0) 
                { 
                    break; 
                }
                else
                {
                    if (max < nums[i])
                    {
                        max = nums[i];
                        index = i;
                    }
                    for (int j = nums.Length - 1; j >= 0; --j)
                    {
                        if (nums[j] == max)
                        {
                            result = j - index;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i > j) matrix[i, j] = 0;
                    else if (j > i) matrix[i, j] = 1;
                }
        }
    }
}
